/** Bài 1
 * - Input: nhập số ngày làm
 * - Process: 
 *  + Khai báo biến số ngày làm , biến lương trên ngày 
 *  + Tính lương = lương trên ngày * số ngày làm
 * - Ouput: xuất lương 
 */
const luong1ngay = 100000;
function TinhLuong() {
    var soNgaylam = document.getElementById("so-ngay-lam").value * 1;
    var tongLuong = luong1ngay * soNgaylam;
    document.getElementById("tienLuong").innerHTML= `<h2 class="text-white bg-black alert-primary">Tổng lương của bạn là : ${tongLuong.toLocaleString()}</h2>`;
}

/** Bài 2
 * - Input : 5 số thực
 * - Process :
 *  + Khai báo biến số thực
 *  + trung bình = tổng 5 số / 5
 * - Output: giá trị trung bình
 */

function TinhTrungBinh() {
    var soThuc1 = document.getElementById("so-thuc-1").value * 1;
    var soThuc2 = document.getElementById("so-thuc-2").value * 1;
    var soThuc3 = document.getElementById("so-thuc-3").value * 1;
    var soThuc4 = document.getElementById("so-thuc-4").value * 1;
    var soThuc5 = document.getElementById("so-thuc-5").value * 1;
    var trungbinh = ( soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5;
    document.getElementById("tinhGiaTri").innerHTML=`<h2 class="text-white bg-black p-3">Giá trị trung bình : ${trungbinh}</h2>`;
}

/** Bài 3
 * - Input : số tiền USD , const = 23.500 VND
 * - Process :
 *  + Khai báo biến số tiền
 *  + Quy đổi = số tiền * const
 * - Output: giá trị tiền VND
 */

const tyGiaUSD = 23500;
function QuyDoi() {
    var soTienUSD = document.getElementById("so-tien-usd").value * 1;
    var soTienVND = soTienUSD * tyGiaUSD;
    document.getElementById("so-tien-vnd").innerHTML=`<h2 class="text-white bg-black alert-warning">Số Tiền VND : ${soTienVND.toLocaleString()}</h2>`;
}

/** Bài 4
 * - Input : chiều dài và chiều rộng
 * - Process :
 *  + Khai báo biến chiều dài và chiều rộng
 *  + Chuvi = (dài + rộng)*2
 *  + Diện tích = dài * rộng
 * - Output: Chu vi và Diện tích
 */

function tinh() {
    var chieuRongHCN = document.getElementById("chieu-rong").value * 1;
    var chieuDaiHCN = document.getElementById("chieu-dai").value * 1;
    var chuViHCN = (chieuDaiHCN + chieuRongHCN) * 2;
    var dienTichHCN = chieuDaiHCN * chieuRongHCN;
    document.getElementById("chuVi").innerHTML= `<h2 class = "text-black">Chu vi HCN = ${chuViHCN}</h2>`;
    document.getElementById("dienTich").innerHTML= `<h2>Diện tích HCN = ${dienTichHCN}</h2>`
}

/** Bài 5
 * - Input : số có 2 chữ số
 * - Process :
 *  + Khai báo biến số đơn vị và số hàng chục
 *  + Tổng 2 ký số= số đvi + số hàng chục
 * - Output: tổng 2 ký số
 */

function tong2kyso() {
    var so2Chuso = document.getElementById("so-2-chu-so").value * 1;
    var so_hang_don_vi = so2Chuso % 10;
    var so_hang_chuc = Math.floor(so2Chuso / 10);
    var tong2Kyso = so_hang_don_vi + so_hang_chuc;
    document.getElementById("tong-2-ky-so").innerHTML=`<h3 class="text-white bg-black"> Tổng 2 ký số : ${tong2Kyso}</h3>`
}